#!/bin/bash
# OTG cloudBOX User Interface for OpenStack
# OTG Computadores Ltda
# November, 02, 2023.
# Version: 1.0
#############################################

getDate() {  # Function name corrected for clarity
  date "+%Y-%m-%dT%H:%M:%S"  # Corrected date format string to ISO 8601
}

echo "OTG cloudBOX User Interface for OpenStack"
if [[ -f "ui-otg.tar" ]]; then  # Corrected file name from "io-otg.tar" to "ui-otg.tar"
  echo "$(getDate) - Extracting ui-otg.tar..."  
  echo "$(getDate) - It will replace these files on your system:"
  tar tvf ui-otg.tar  
  read -p "Are you sure to replace the files showed above? (Y/N): " opt
  if [[ "$opt" == "Y" || "$opt" == "y" ]]; then  
    tar xvf ui-otg.tar  
    if [[ -d opt ]]; then
      # Loop through the files in the 'opt' directory
      find opt -type f | while read file; do
      echo "$(getDate) - Replacing '/$file'"
        rm "/$file"  # Remove the existing file
        mv "$file" "/$file"  # Move the new file to the intended location
      done
    fi
    echo "$(getDate) - Restarting Apache Web Server" 
    sudo service apache2 restart
    echo "$(getDate) - DONE!"
  else
    echo "$(getDate) - Aborted!"
  fi  
else
  echo "$(getDate) - File ui-otg.tar not found!"
fi  
